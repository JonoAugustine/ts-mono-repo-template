# On It - Server

This project contains the monolithic server/back-end for *On It*.
The deployed server itself as well as scripts for database
configuration.